#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import bottle
import argparse
import json
from services import feedwrangler
import filterconfig


def setup_routes(config, connector):
    @bottle.route('/<filename:path>')
    def send_static(filename):
        return bottle.static_file(filename, root='webfront')

    @bottle.route('/')
    def index():
        return bottle.static_file("index.html", root='webfront')

    @bottle.post('/saveFilters')
    def save_filters():
        filter_items = json.loads(bottle.request.forms.get("expressions"))
        config.filter_items = filter_items

    @bottle.get('/filteritems')
    def filter_items():
        return json.dumps(config.filter_items)

    @bottle.get('/feeditems')
    def feeditems():
        articles = connector.fetch_articles()

        return json.dumps(articles)


def parse_cli_arguments():
    arguments = {
        "path": ""
    }

    parser = argparse.ArgumentParser(description='RSS feed filter Web UI',
                                     prog='RSS Content Filter Web UI')
    parser.add_argument('-p', '--port',
                        help='The port to open the webserver at.')
    parser.add_argument('-s', '--service',
                        required=True,
                        help='The name of the feed provider')
    parser.add_argument('--config',
                        help="Optional path of the config file.")

    args = parser.parse_args()

    if args.port:
        arguments['port'] = args.port

    if args.service:
        arguments['service_name'] = args.service

    if args.config:
        arguments['path'] = args.config

    return arguments


def main():
    cli_args = parse_cli_arguments()
    config = filterconfig.FilterConfig(path=cli_args['path'],
                                       service_name=cli_args['service_name'])

    connector = feedwrangler.Connector(config.access_token)

    setup_routes(config, connector)

    bottle.run(
        host='localhost',
        port=cli_args['port'] if "port" in cli_args.keys() else config.port
    )


if __name__ == '__main__':
    main()
