# RSS feed content filter

Filter (mark as read) certain posts coming from services like FeedWrangler.

At the moment regular expressions are matched against the heading / title of the posts.

## Supported services right now:

* [FeedWrangler](https://feedwrangler.net)
