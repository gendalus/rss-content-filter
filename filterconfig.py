#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os.path


class FilterConfig(object):
    """docstring for FilterConfig"""
    def __init__(self, **kwargs):
        # Service name has to be here because it is required from the cli
        if "service_name" in kwargs.keys():
            self.service_name = kwargs['service_name']

        if "path" not in kwargs.keys() or kwargs['path'] == "":
            self.config_path = "{home}/{filename}".format(
                home=os.path.expanduser("~"),
                filename="/.feed-content-filter.cnf"
            )
        else:
            self.config_path = kwargs['path']

        # Check if config is created yet
        if os.path.isfile(self.config_path):
            self.load()
        else:
            if not self.should_create_config_template():
                print("Operation could not take place without config file.")
                quit()
            else:
                if self.create_config_template():
                    self.load()
                else:
                    quit()

    def load(self):
        with open(self.config_path) as data_file:
            self._data = json.load(data_file)

    def save(self):
        with open(self.config_path, 'w') as data_file:
            json.dump(self._data, data_file, indent=4)

    def should_create_config_template(self):
        msg = "Config not found. Create empty config file? [Y]es / [N]o: "

        should_create = input(msg)

        if should_create not in ["y", "Y", "n", "N", True, False]:
            should_create = self.should_create_config_template()

        if should_create in [True, False]:
            return should_create

        if should_create in ["y", "Y"]:
            return True

        if should_create in ["n", "N"]:
            return False

    def create_config_template(self):
        port = 8080
        access_token = ""

        input_port = input("Port to use for the webserver [default 8080]: ")

        if input_port != "":
            port = input_port

        access_token = input("Access token for {service}. Can be added / changed later: ".format(service=self.service_name))

        template_values = {
            "webui": {
                "port": port
            },
            "services": {
                self.service_name: {
                    "access_token": access_token,
                    "filter_items": []
                }
            }
        }

        try:
            with open(self.config_path, 'w') as conf_file:
                json.dump(template_values, conf_file, indent=4)
            return True

        except Exception as e:
            print("Config file could not be saved:")
            print(e)
            return False

    @property
    def access_token(self):
        return self._data['services'][self.service_name]['access_token']

    @property
    def filter_items(self):
        return self._data['services'][self.service_name]['filter_items']

    @filter_items.setter
    def filter_items(self, value):
        self._data['services'][self.service_name]['filter_items'] = value
        self.save()

    @property
    def port(self):
        return self._data['webui']['port']
