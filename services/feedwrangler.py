#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests


class Connector(object):
    """Feed connector for feedwrangler"""
    def __init__(self, api_key, verbosity=0):
        self.api_key = api_key
        self.verbosity = verbosity

    def fetch_articles(self, page=0):
        articles = []
        feed_items = []
        additional_articles = []

        try:
            response = requests.get(
                url="https://feedwrangler.net/api/v2/feed_items/list",
                params={
                    "access_token": self.api_key,
                    "read": "false",
                    "offset": page * 100,
                },
            )

            if response.status_code == 200:
                feed_items = response.json()['feed_items']

        except requests.exceptions.RequestException:
            print('HTTP Request failed')

        for item in feed_items:
            articles.append({
                "id": item['feed_item_id'],
                "title": item['title']
            })

        if len(articles) == 100:
            additional_articles = self.fetch_articles(page + 1)

        articles = articles + additional_articles

        return articles

    def mark_as_read(self, item_ids):
        item_ids_string = ""

        for item_id in item_ids:
            item_ids_string += str(item_id) + ","

        item_ids_string = item_ids_string[0:len(item_ids_string) - 1]

        try:
            response = requests.get(
                url="https://feedwrangler.net/api/v2/feed_items/mark_all_read",
                params={
                    "access_token": self.api_key,
                    "feed_item_ids": item_ids_string,
                },
            )

            if response.status_code != 200:
                print('Marking as read failed')

            if self.verbosity > 0:
                print("{0} article(s) marked as read.".format(len(item_ids)))

        except requests.exceptions.RequestException:
            print('Marking as read failed')
