#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import re
from services import feedwrangler
import filterconfig


class FilterMatcher(object):
    """FilterMatcher matches items against filter items"""
    def __init__(self, filter_items=[], verbosity=0):
        self.filter_items = filter_items
        self.verbosity = verbosity

    def does_filter_match(self, item):
        if self.verbosity > 0:
            print("Testing: \"%s\"" % item)

        for expression in self.filter_items:
            pattern = re.compile(expression)

            match = pattern.search(item)

            if match:
                if self.verbosity > 1:
                    print("+ | \"%s\" did match" % expression)
                return True

            else:
                if self.verbosity > 1:
                    print("- | \"%s\" did not match" % expression)

        return False


def parse_cli_arguments():
    arguments = {
        "verbosity": 0,
        "simulate": False,
        "path": ""
    }

    parser = argparse.ArgumentParser(description='A RSS feed filter',
                                     prog='RSS Content Filter')
    parser.add_argument('-v', '--verbosity',
                        help='How talky should it be?')
    parser.add_argument('-s', '--simulate',
                        default=False,
                        nargs="?",
                        const=True,
                        help='Do everything but the marking as read')
    parser.add_argument('-p', '--provider',
                        required=True,
                        help='The name of the feed provider')
    parser.add_argument('--config',
                        help="Optional path of the config file.")
    args = parser.parse_args()

    if args.verbosity:
        arguments['verbosity'] = int(args.verbosity)

    if args.simulate:
        arguments['simulate'] = True

    if args.provider:
        arguments["service_name"] = args.provider

    if args.config:
        arguments['path'] = args.config

    if arguments['verbosity'] > 0:
        print("CLI arguments: {args}".format(args=arguments))

    return arguments


def main():
    cli_args = parse_cli_arguments()

    config = filterconfig.FilterConfig(path=cli_args['path'],
                                       service_name=cli_args['service_name'])

    matching_ids = []

    connector = feedwrangler.Connector(config.access_token)
    matcher = FilterMatcher(config.filter_items, cli_args['verbosity'])

    articles = connector.fetch_articles()

    if cli_args['verbosity'] > 0:
        print("{0} Articles fetched".format(len(articles)))

    for article in articles:
        if matcher.does_filter_match(article['title']):
            matching_ids.append(article['id'])

    if len(matching_ids) > 0 and not cli_args['simulate']:
        connector.mark_as_read(matching_ids)


if __name__ == '__main__':
    main()
