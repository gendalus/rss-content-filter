function FilterList(filterItems) {
    this._items = [];

    for (var i = 0; i < filterItems.length; i++) {
        this.addFilterItem(filterItems[i]);
    }
}

FilterList.prototype.addFilterItem = function(filterItem) {
    let newFilterItem = new FilterItem(this._items.length, filterItem);
    this._items.push(newFilterItem);
};

FilterList.prototype.removeItem = function(id) {
    this._items[id].view.remove();
    this._items.splice(id, 1);

    for (var i = 0; i < this._items.length; i++) {
        this._items[i].id(i);
    }
};

FilterList.prototype.items = function(){
    return this._items;
};

FilterList.prototype.itemForId = function(id) {
    return this._items[id];
};

function FilterItem(id, expression) {
    this._id = id;
    this._expression = expression;
    this.view = new FilterView(this._id, expression);
}

FilterItem.prototype.id = function(id) {
    if(id === undefined){
        return this._id;
    }else{
        this._id = id;
        this.view.updateId(id);
    }
};

FilterItem.prototype.expression = function(expression) {
    if(expression === undefined){
        return this._expression;
    }else{
        this._expression = expression;
    }
};

function FilterView(id, expression) {
    this.id = id;
    this._expression = expression;

    $('#filterList').append(this.html());

    $(this.containerId()).find("button").on("click", function(event) {
        let senderId = $(event.currentTarget).parent().parent().parent().attr("id");
        let filterItemId = senderId.substr(6, senderId.length);

        document.fl.removeItem(filterItemId);

        document.matcher.matchArticles();
    });

    this.input().on("change", function(event) {
        let senderId = $(event.currentTarget).parent().parent().attr("id");
        let filterItemId = senderId.substr(6, senderId.length);
        let newExpression = $(event.currentTarget).val();

        document.fl.itemForId(filterItemId).expression(newExpression);

        document.matcher.matchArticles();
    });
}

FilterView.prototype.input = function() {
    return $(this.containerId()).find("input");
};

FilterView.prototype.containerId = function() {
    return "#filter" + this.id;
};

FilterView.prototype.expression = function(expression) {
    if(expression === undefined){
        return this._expression;
    }else{
        this._expression = expression;
        this.input().val(expression);
    }
};

FilterView.prototype.html = function() {
    html = `
    <li class="list-group-item" id="filter${this.id}">
        <div class="input-group input-group-sm">
            <div class="input-group-addon"><span># ${this.id + 1}</span></div>
            <input class="form-control" type="text" value="${this.expression()}">
            <div class="input-group-btn">
                <button class="btn btn-danger" type="button"><i class="glyphicon glyphicon-remove"></i></button>
            </div>
        </div>
    </li>`;

    return html;
};

FilterView.prototype.updateId = function(id) {
    $(`#filter${this.id}`).find("span").text(`# ${id + 1}`);
    $(`#filter${this.id}`).attr("id", "filter" + id);
    this.id = id;
};

FilterView.prototype.remove = function() {
    $('#filter' + this.id).remove();
};

function Article(id, name) {
    this.name = name;
    this.id = id;

    this.view = new ArticleView(this.id, this.name);
}

Article.prototype.matched = function(isMatched) {
    if(typeof(isMatched) === "boolean"){
        this.view.matched(isMatched);
    }
};

function ArticleView(id, name) {
    this.name = name;
    this.id = id;

    $('#articleList').append(this.html());
}

ArticleView.prototype.matched = function(isMatched) {
    if(typeof(isMatched) !== "boolean") {
        throw("Wrong input type. Is matched has to be boolean");
    }

    $('#article' + this.id).replaceWith(this.html({"matched": isMatched}));
};


ArticleView.prototype.html = function(args) {
    var matched = false

    if(args !== undefined){
        if(args['matched'] !== undefined) {
            matched = args['matched'];
        }
    }

    if(matched) {
        return `<li id="article${this.id}" class="list-group-item list-group-item-success"><span>${this.name}</span></li>`;
    }else{
        return `<li id="article${this.id}" class="list-group-item"><span>${this.name}</span></li>`;
    }
};


function ArticleController(feedItems) {
    this._articles = [];

    for (var i = 0; i < feedItems.length; i++) {
        this.addArticle(feedItems[i]);
    }
}

ArticleController.prototype.replaceArticles = function(feedItems) {
    $('#articleList').empty();
    this._articles = [];

    for (var i = 0; i < feedItems.length; i++) {
        this.addArticle(feedItems[i]);
    }

    document.matcher.matchArticles();
};

ArticleController.prototype.addArticle = function(feedItem) {
    let article = new Article(feedItem['id'],
                              feedItem['title'])

    this._articles.push(article);
};

ArticleController.prototype.articles = function() {
    return this._articles;
};

function Matcher() {
    // this.matchArticles();
}

Matcher.prototype.matchArticles = function() {
    this.expressions = [];

    let filterItems = document.fl.items();

    for (var i = 0; i < filterItems.length; i++) {
        this.expressions.push(filterItems[i].expression());
    }

    let articles = document.ac.articles();

    for (var i = 0; i < articles.length; i++) {
        if(this.doesMatch(articles[i].name)){
            articles[i].matched(true);
        }else{
            articles[i].matched(false);
        }
    }
};

Matcher.prototype.doesMatch = function(item){
    for (var i = 0; i < this.expressions.length; i++) {
        let expression = new RegExp(this.expressions[i]);

        if(expression !== ""){
            if(item.match(expression) !== null){
                return true;
            }
        }
    }

    return false;
};

let feedItems = [
    {
      "id": 2573752025,
      "title": "Mehr als 100 Tote bei Angriff auf afghanische Milit\u00e4rbasis in Nordafghanistan (neue Zahlen, Nachtr\u00e4ge)"
    },
    {
      "id": 2573602406,
      "title": "How Apple Won Silicon: Why Galaxy S8 Can\u2019t Go Core-to-Core With iPhone 7"
    },
    {
      "id": 2573603000,
      "title": "New Apple Watch NikeLab"
    },
    {
      "id": 2573513847,
      "title": "Apple announces iTunes' top five best-selling holiday movies of all time"
    },
    {
      "id": 2573513744,
      "title": "Super Mario debuts on iPhone and iPad"
    },
    {
      "id": 2573513641,
      "title": "Apple adds hundreds of new and redesigned emoji in iOS 10.2"
    },
    {
      "id": 2573513538,
      "title": "Apple AirPods are now available"
    },
    {
      "id": 2573513435,
      "title": "Pro photo tips for using Portrait mode on iPhone 7 Plus"
    },
    {
      "id": 2573513332,
      "title": "Apple unveils Best of 2016 across apps, music, movies and more"
    },
    {
      "id": 2573513229,
      "title": "Apple stores go (RED) for World AIDS Day"
    },
    {
      "id": 2573513126,
      "title": "Apple March\u00e9 Saint-Germain opens in Paris"
    },
    {
      "id": 2573513023,
      "title": "Apple turns (RED) with more ways than ever to join the fight against AIDS"
    },
    {
      "id": 2573512920,
      "title": "Free Hour of Code workshops December 5 through 11 at every Apple Store"
    },
    {
      "id": 2573512817,
      "title": "Apple and The Conservation Fund advance forest protection efforts"
    },
    {
      "id": 2573512714,
      "title": "A touch of giving with Apple Pay "
    },
    {
      "id": 2573512611,
      "title": "\u201cDesigned by Apple in California\u201d chronicles 20 years of Apple design"
    },
    {
      "id": 2573512508,
      "title": "Highlights from Apple\u2019s October 2016 event"
    },
    {
      "id": 2573512405,
      "title": "Apple releases significant update to Final Cut Pro X"
    },
    {
      "id": 2573512302,
      "title": "Apple unveils new TV app for Apple TV, iPhone and iPad"
    },
    {
      "id": 2573512199,
      "title": "Apple unveils groundbreaking new MacBook Pro"
    },
    {
      "id": 2573512096,
      "title": "Apple Reports Fourth Quarter Results"
    },
    {
      "id": 2573511993,
      "title": "Portrait mode now available on iPhone 7 Plus with iOS 10.1"
    },
    {
      "id": 2573511890,
      "title": "Apple Watch Nike+, the perfect running partner, arrives Friday, October 28"
    },
    {
      "id": 2573511787,
      "title": "Apple FY 16 Fourth Quarter Results Conference Call"
    },
    {
      "id": 2573511684,
      "title": "Apple Regent Street to reopen with new design"
    },
    {
      "id": 2573511581,
      "title": "Apple opens first iOS Developer Academy in Naples"
    },
    {
      "id": 2573511478,
      "title": "This week's highlights from Apple Music Festival 10"
    },
    {
      "id": 2573511375,
      "title": "A Game of Thrones: Enhanced Edition comes exclusively to iBooks"
    },
    {
      "id": 2573511272,
      "title": "Apple and Deloitte team up to accelerate business transformation on\u00a0iPhone and iPad"
    },
    {
      "id": 2573511169,
      "title": "Apple's grand opening in Mexico"
    },
    {
      "id": 2573511066,
      "title": "Apple to open first store in Mexico Saturday"
    },
    {
      "id": 2573510963,
      "title": "Highlights from Apple Music Festival 10"
    },
    {
      "id": 2573510860,
      "title": "macOS Sierra now available as a free update"
    },
    {
      "id": 2573510757,
      "title": "Apple joins RE100, announces supplier clean energy pledges"
    },
    {
      "id": 2573510654,
      "title": "Stickers take iPhone by storm"
    },
    {
      "id": 2573510551,
      "title": "iPhone 7 and Apple Watch Series 2 arrive in stores"
    },
    {
      "id": 2573510448,
      "title": "What\u2019s new in iOS 10"
    },
    {
      "id": 2573510345,
      "title": "Swift Playgrounds now available on the App Store"
    },
    {
      "id": 2573510242,
      "title": "Highlights from Apple\u2019s September 2016 event"
    },
    {
      "id": 2573510139,
      "title": "Apple Pay coming to Japan with iPhone 7"
    },
    {
      "id": 2573510036,
      "title": "Apple Watch Herm\u00e8s introduces new styles \u0026 colors"
    },
    {
      "id": 2573509933,
      "title": "Apple reinvents the wireless headphone with AirPods"
    },
    {
      "id": 2573509830,
      "title": "Apple \u0026 Nike launch Apple Watch Nike+"
    },
    {
      "id": 2573509727,
      "title": "Apple introduces Apple Watch Series 2"
    },
    {
      "id": 2573509624,
      "title": "Apple introduces iPhone 7 \u0026 iPhone 7 Plus"
    },
    {
      "id": 2573509521,
      "title": "Hey Siri, send John $10"
    },
    {
      "id": 2573509418,
      "title": "Hey Siri, book me a ride"
    },
    {
      "id": 2573509315,
      "title": "Hey Siri, show me my best selfies"
    },
    {
      "id": 2573509212,
      "title": "Apple Music Festival to light up London this September"
    },
    {
      "id": 2573509109,
      "title": "Apple announces environmental progress in China"
    },
    {
      "id": 2573509006,
      "title": "Manhattan\u2019s newest Apple Store opens at World Trade Center"
    },
    {
      "id": 2573508903,
      "title": "Apple\u2019s ConnectED efforts reach more than 32,000 students"
    },
    {
      "id": 2573508800,
      "title": "Apple celebrates diversity with \u201cThe Human Family\u201d"
    },
    {
      "id": 2573508697,
      "title": "Apple adds more gender diverse emoji in iOS 10"
    },
    {
      "id": 2573508594,
      "title": "Celebrating Brooklyn\u2019s first Apple Store "
    },
    {
      "id": 2573508491,
      "title": "Apple Williamsburg opens in Brooklyn on Saturday"
    },
    {
      "id": 2573508388,
      "title": "Apple celebrates one billion iPhones"
    },
    {
      "id": 2573508285,
      "title": "Apple Reports Third Quarter Results"
    },
    {
      "id": 2573508182,
      "title": "Apple FY 16 Third Quarter Results Conference Call"
    },
    {
      "id": 2573508079,
      "title": "Apple Pay Now Available in Switzerland "
    },
    {
      "id": 2573507976,
      "title": "Apple \u0026 Donate Life America bring organ donation to iPhone"
    },
    {
      "id": 2573507873,
      "title": "Global Apps for Earth campaign with WWF raises more than $8M"
    },
    {
      "id": 2573507770,
      "title": "Hello from WWDC16"
    },
    {
      "id": 2573507667,
      "title": "Apple previews iOS 10, biggest iOS release ever"
    },
    {
      "id": 2573507564,
      "title": "Apple previews major update with macOS Sierra"
    },
    {
      "id": 2573507461,
      "title": "Apple previews watchOS 3"
    },
    {
      "id": 2573507358,
      "title": "Apple TV gets new Siri capabilities and single sign-on"
    },
    {
      "id": 2573507255,
      "title": "Swift Playgrounds app makes learning to code easy and fun"
    },
    {
      "id": 2573507152,
      "title": "Apple celebrates Earth Day with new initiatives and stories of innovation"
    },
    {
      "id": 2573507049,
      "title": "iPad Pro artist illustrates NBA stars"
    },
    {
      "id": 2573506946,
      "title": "Clips brings fun and simple video creation to iPhone and iPad"
    },
    {
      "id": 2573506843,
      "title": "Apple introduces Clips: the fun, new way to create expressive videos on iOS"
    },
    {
      "id": 2573506740,
      "title": "Apple opens new stores in Nanjing, Cologne and Miami"
    },
    {
      "id": 2573506637,
      "title": "Swift Playgrounds now available in five additional languages"
    },
    {
      "id": 2573506534,
      "title": "Apple takes supplier clean energy program to Japan"
    },
    {
      "id": 2573506431,
      "title": "Raising the bar: Apple\u2019s 11th annual Supplier Responsibility progress report released"
    },
    {
      "id": 2573506328,
      "title": "Apple Watch band offerings expand for spring 2017"
    },
    {
      "id": 2573506225,
      "title": "New 9.7-inch iPad features stunning Retina display and incredible performance"
    },
    {
      "id": 2573506122,
      "title": "Apple introduces iPhone 7 and iPhone 7 Plus (PRODUCT)RED Special Edition"
    },
    {
      "id": 2573506019,
      "title": "Apple Park opens to employees in April"
    },
    {
      "id": 2573505916,
      "title": "Apple\u2019s WWDC returns to San Jose June 5-9, 2017"
    },
    {
      "id": 2573505813,
      "title": "Apple Reports Record First Quarter Results"
    },
    {
      "id": 2573505710,
      "title": "Apple FY 17 First Quarter Results Conference Call"
    },
    {
      "id": 2573505607,
      "title": "Hey Siri, who\u2019s going to win the Super Bowl?"
    },
    {
      "id": 2573505504,
      "title": "Behind Apple\u2019s new campaign: One Night on iPhone 7"
    },
    {
      "id": 2573505401,
      "title": "GarageBand and Logic Pro X music apps get major updates"
    },
    {
      "id": 2573505298,
      "title": "iPhone at ten: the revolution continues"
    },
    {
      "id": 2573505195,
      "title": "App Store shatters records on New Year\u2019s Day"
    },
    {
      "id": 2573487241,
      "title": "Apple\u2019s New Earth Day Videos"
    },
    {
      "id": 2573453804,
      "title": "One podcaster's (fruitless) quest to replace Skype\n"
    },
    {
      "id": 2573323779,
      "title": "The Talk Show: Apple VP Lisa Jackson"
    },
    {
      "id": 2573324361,
      "title": "Speed Test Between iPhone 7 Plus and Top Android Phones"
    },
    {
      "id": 2573148422,
      "title": "Updates on Airfoil and the Newest Apple TVs"
    },
    {
      "id": 2572964310,
      "title": "Setapp Update"
    },
    {
      "id": 2573003598,
      "title": "DPRK Wound Filament Airframes"
    },
    {
      "id": 2572936334,
      "title": "The Natural History Collection from the Curious Card Company"
    },
    {
      "id": 2572803954,
      "title": "Bardet \u003Cb\u003ERoma\u003C/b\u003E. gegen Uran Rigobe. 23.04.2017 LiegeBastogneLiege H2H"
    },
    {
      "id": 2572685805,
      "title": "Chase + Scout Tiny Dagger Rings + a Haute Macabre Discount"
    },
    {
      "id": 2573045740,
      "title": "\u00abWaren fast \u00fcbermotiviert\u00bb \u2013 Solothurner Polizeihunde-Teams d\u00fcmpeln auf hinteren R\u00e4ngen"
    },
    {
      "id": 2573045741,
      "title": "Luftpostkarte 1 volo postale Swissair Locarno - \u003Cb\u003ERoma\u003C/b\u003E 18.3.40"
    }
];

$('#addFilterButton').on("click", function(){
    document.fl.addFilterItem("");
});

$('#saveButton').on("click", function() {
    $('#saveButton').attr("disabled", "disabled");
    let filterItems = document.fl.items();
    var expressions = [];

    for (var i = 0; i < filterItems.length; i++) {
        expressions.push(filterItems[i].expression())
    }

    if(expressions.length > 0) {
        var jqxhr = $.ajax({
            "url": "saveFilters",
            "method": "POST",
            "data": {
                "expressions": JSON.stringify(expressions)
            },
        })
        .done(function() {
            console.log("success");
        })
        .fail(function() {
            alert("Saving filter failed");
        })
        .always(function() {
            $('#saveButton').removeAttr("disabled");
        });
    }
});

$('#reloadButton').on("click", function() {
    $('#reloadButton').attr("disabled", "disabled");
    fetchArticles();
});

function fetchFilterItems() {
    var jqxhr = $.ajax({
        "url": "filteritems",
        "dataType": "json"
    })
    .done(function(result) {
        if(document.fl === undefined){
            document.fl = new FilterList(result);
        }else{}

        initialiseArticles();
    })
    .fail(function() {
        alert( "Filter items could not be loaded." );
    })
      .always(function() {
    });
}

function fetchArticles() {
    var jqxhr = $.ajax({
        "url": "feeditems",
        "dataType": "json"
    })
    .done(function(result) {
        document.ac.replaceArticles(result);
    })
    .fail(function() {
        alert( "RSS item could not be loaded." );
    })
    .always(function() {
        $('#reloadButton').removeAttr("disabled");
    });
}

function initialiseArticles(){
    document.ac = new ArticleController([]);
    fetchArticles();
}

$(document).ready(function() {
    document.matcher = new Matcher();

    fetchFilterItems();
});
